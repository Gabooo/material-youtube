import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomersRoutingModule } from './customers-routing.module';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { CutomerCardComponent } from './cutomer-card/cutomer-card.component';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [CustomerListComponent, CutomerCardComponent],
  imports: [
    CommonModule,
    CustomersRoutingModule,
    //Material
    MatIconModule, MatButtonModule, MatCardModule
  ]
})
export class CustomersModule { }
